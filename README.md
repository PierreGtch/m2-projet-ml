# Paris Fire Brigade Challenge

This repository contains the source code used to compete in the Paris Fire Brigade data challenge.
 - the challenge presentation : https://paris-fire-brigade.github.io/data-challenge/challenge.html
 - the public ranking : https://challengedata.ens.fr/participants/challenges/21/ranking/public


net1.ipynb and neural_net_0.ipynb are both attempts using neural networks.

boosting_0.ipynb is a code using a boosting method that predicts the `delta selection-departure` feature.

boosting_1.ipynb is a code using a boosting method that predicts the `delta departure-presentation` feature.

For more details on the preprocessing or the models used, please refere to report.md
